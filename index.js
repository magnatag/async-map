module.exports = function (arr, asyncFunction) {
	return Promise.all(arr.map(async (arrEl) => {
		return await asyncFunction(arrEl);
	}));
};